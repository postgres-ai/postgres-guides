[TOC]

# SQL Style Guide
This style guide is derived from [Firefox Data Documentation](https://docs.telemetry.mozilla.org/concepts/sql_style.html).

The only difference is that **we use lowercased SQL**, which is less common than uppercased.

Why lowercased? We tend to think about SQL and PL/pgSQL as very powerful programming language. In most cases, we do not use it as embedded to code written in another language, so we do not have reasons to distinguish it. Upper case looks old-fashioned and requires using Caps Lock or Shift, which is inconvenient. At the same time, it might still make sense to use uppercase SQL keywords when using them in, for example, natural language texts, for example:
>  .. using multiple JOINs ..

## Consistency
From [Pep8](https://www.python.org/dev/peps/pep-0008/#a-foolish-consistency-is-the-hobgoblin-of-little-minds):

> A style guide is about consistency.
> Consistency with this style guide is important.
> Consistency within a project is more important.
> Consistency within one module or function is the most important.
>
> However, know when to be inconsistent --
> sometimes style guide recommendations just aren't applicable.
> When in doubt, use your best judgment.
> Look at other examples and decide what looks best.
> And don't hesitate to ask!

## Reserved words
Always use <s>upper</s>**lowercase** for reserved keywords like `select`, `where`, or `as`.

## Variable names
1. Use consistent and descriptive identifiers and names.
1. Use lower case names with underscores, such as `first_name`.
   **Do not use CamelCase.**
1. Functions, such as `cardinality`, `approx_distinct`, or `substr`,
   [are identifiers](https://www.postgresql.org/docs/current/static/sql-syntax-lexical.html#SQL-SYNTAX-IDENTIFIERS)
   and should be treated like variable names.
1. Names must begin with a letter and may not end in an underscore.
1. Only use letters, numbers, and underscores in variable names.

## Be explicit
When choosing between explicit or implicit syntax, prefer explicit.

### aliasing
Always include the `as` keyword when aliasing a variable or table name,
it's easier to read when explicit.

**Good**
```sql
select date(t.created_at) as day
from telemetry as t
limit 10;
```

**Bad**
```sql
select date(t.created_at) day
from telemetry t
limit 10;
```

### JOINs
Always include the `join` type rather than relying on the default join.

**Good**
```sql
select
  submission_date,
  experiment.key as experiment_id,
  experiment.value as experiment_branch,
  count(*) as count
from
  telemetry.clients_daily
cross join
  unnest(experiments.key_value) as experiment
where
  submission_date > '2019-07-01'
  and sample_id = '10'
group by
  submission_date,
  experiment_id,
  experiment_branch;
```

**Bad**
```sql
select
  submission_date,
  experiment.key as experiment_id,
  experiment.value as experiment_branch,
  count(*) as count
from
  telemetry.clients_daily,
  unnest(experiments.key_value) as experiment -- Implicit JOIN
where
  submission_date > '2019-07-01'
  and sample_id = '10'
group by
  1, 2, 3; -- Implicit grouping column names
```

### Grouping columns
Implicit grouping columns should be avoided. However, if grouping is done on an expression it is acceptable to use implicit grouping columns to avoid repeating repeating the expressions.

**Good**
```sql
select state, backend_type, count(*)
from pg_stat_activity
group by state, backend_type
order by state, backend_type;
```

**Acceptable**
```sql
select
  date_trunc('minute', xact_start) as xact_start_minute,
  count(*)
from pg_stat_activity
group by 1
order by 1;
```

**Bad**
```sql
select state, backend_type, count(*)
from pg_stat_activity
group by 1, 2
order by 1, 2;
```

## Left-align root keywords
Root keywords should all start on the same character boundary.

**Good**:

```sql
select
  client_id,
  submission_date
from main_summary
where
  sample_id = '42'
  and submission_date > '20180101'
limit 10;
```

**Bad**:

```sql
select client_id,
       submission_date
  from main_summary
 where sample_id = '42'
   and submission_date > '20180101';
```

## Code blocks
Root keywords should be on their own line in all cases except when followed by only one dependant word. If the number of dependant words is more than one, they should form a column that is left-aligned and indented to the left with respect to the root keyword.

**Good**:
```sql
select
  client_id,
  submission_date
from main_summary
where
  submission_date > '20180101'
  and sample_id = '42'
limit 10;
```

It's acceptable to include an argument on the same line as the root keyword, if there is exactly one argument.

**Acceptable**:
```sql
select
  client_id,
  submission_date
from main_summary
where
  submission_date > '20180101'
  and sample_id = '42'
limit 10;
```

Do not include multiple arguments on one line.

**Bad**:
```sql
select client_id, submission_date
from main_summary
where submission_date > '20180101' and (sample_id = '42' or sample_id is null)
limit 10;
```

**Bad**
```sql
select
  client_id,
  submission_date
from main_summary
where submission_date > '20180101'
  and sample_id = '42'
limit 10;
```

## Parentheses
If parentheses span multiple lines:
1. The opening parenthesis should terminate the line.
1. The closing parenthesis should be lined up under
   the first character of the line that starts the multi-line construct.
1. The contents of the parentheses should be indented one level.

For example:
**Good**
```sql
with sample as (
  select
    client_id,
    submission_date
  from
    main_summary
  where
    sample_id = '42'
)
...
```

**Bad** (terminating parenthesis on shared line)
```sql
with sample as (
  select
    client_id,
    submission_date
  from
    main_summary
  where
    sample_id = '42')
...
```

**Bad** (no indent)
```sql
with sample as (
select
  client_id,
  submission_date
from
  main_summary
where
  sample_id = '42'
)
...
```

## Boolean at the beginning of line
`and` and `or` should always be at the beginning of the line.

**Good**
```sql
...
where
  submission_date > 20180101
  and sample_id = '42'
```

**Bad**
```sql
...
where
  submission_date > 20180101 and
  sample_id = '42'
```

## Nested queries
Do not use nested queries. Instead, use common table expressions to improve readability.

**Good**:
```sql
with sample as (
  select
    client_id,
    submission_date
  from
    main_summary
  where
    sample_id = '42'
)
select *
from sample
limit 10;
```

**Bad**:
```sql
select *
from (
  select
    client_id,
    submission_date
  from
    main_summary
  where
    sample_id = '42'
)
limit 10;
```

## About this Document
This document was heavily influenced by https://www.sqlstyle.guide/ and https://docs.telemetry.mozilla.org/concepts/sql_style.html.

Changes to the style guide should be reviewed by at least one Postgres expert.

